Vue.filter('formatDate', function(value) {
  const options = { year: 'numeric', month: 'numeric', day: 'numeric' };
  const formattedDate = new Date(value).toLocaleDateString('en-US', options);
  const formattedTime = new Date(value).toLocaleTimeString('en-US');
  return `${formattedDate} at ${formattedTime}`;
});
