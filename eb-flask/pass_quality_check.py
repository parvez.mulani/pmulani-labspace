def is_weak_password(password):
    min_length = 8
    max_length = 20
    has_uppercase = False
    has_lowercase = False
    has_digit = False
    has_special_char = False

    if len(password) < min_length or len(password) > max_length:
        return True

    for char in password:
        if char.isupper():
            has_uppercase = True
        elif char.islower():
            has_lowercase = True
        elif char.isdigit():
            has_digit = True
        else:
            has_special_char = True

    if not has_uppercase or not has_lowercase or not has_digit or not has_special_char:
        return True

    return False
