from flask import Flask, request, render_template, jsonify, redirect, url_for, session
from functools import wraps
import jwt
from dotenv import load_dotenv
import os
import json
from pass_quality_check import is_weak_password
from flask_sqlalchemy import SQLAlchemy
# from json_to_database import json_getter
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
import secrets
#load_dotenv('/Users/firat/Desktop/CoinMetrics/Projects/customer_support_hexowebhook/webhook/hexo_app.env')




SECRET_KEY = os.getenv('AUTH_TOKEN')

USERNAME = os.getenv('USERNAME')
PASSWORD = os.getenv('PASSWORD')

#app = Flask(__name__, template_folder='/path/to/your/templates/folder')

application = Flask(__name__)
application.config['EXPLAIN_TEMPLATE_LOADING'] = True
application.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///UserInformation.db'
application.config['SQLALCHEMY_BINDS'] = {'secondary': 'sqlite:///PreviousData.db'}
db = SQLAlchemy(application)

# limiter = Limiter(get_remote_address, app=app, storage_uri='redis://localhost:6379/0', default_limits=["200 per day", "50 per hour"])
# redis = Redis(host='localhost', port=6379)

application.secret_key = secrets.token_hex(16) # Set a secret key for session security

exchange_data = []  # Store webhook data in a list
node_data = []  # Store node data in a list


# Database start

class SecondaryModel(db.Model):
    __bind_key__ = 'secondary'
    __tablename__ = 'Historical Data'
    exch_id = db.Column(db.Integer, primary_key=True)
    exch_title = db.Column(db.String(500), nullable=False)
    exch_name = db.Column(db.String(500), nullable=False)
    exch_monitoredUrl = db.Column(db.String(500), nullable=False)
    exch_createDate = db.Column(db.String(500), nullable=False)
    exch_date = db.Column(db.String(500), nullable=False)
    exch_link = db.Column(db.String(500), nullable=False)
    exch_diffData = db.Column(db.String(500), nullable=False)
    exch_newData = db.Column(db.String(500), nullable=False)
    exch_oldData = db.Column(db.String(500), nullable=False)

    def __repr__(self):
        return '<Exchange %r>' % self.exch_id


# Database end

# User DATABASE START

class UserInformation(db.Model):
    __tablename__ = 'User Database'
    user_id = db.Column(db.Integer, primary_key=True)
    user_name = db.Column(db.String(100), unique=True)
    user_email = db.Column(db.String(200), unique=True, nullable=False)
    user_password = db.Column(db.String(16), default=0)

    def __repr__(self):
        return '<User %r>' % self.user_email


# DATABASE END


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if 'username' in session:
            # Check if Authorization header is present
            if 'Authorization' in request.headers:
                # Extract the token from the Authorization header
                token = request.headers['Authorization'].split()[1]
                try:
                    # Verify and decode the token
                    jwt.decode(token, secrets.token_hex(16), algorithms=['HS256'])
                    # If the token is valid, proceed with the decorated function
                    return f(*args, **kwargs)
                except jwt.ExpiredSignatureError:
                    return 'Token expired. Please log in again.', 401
                except jwt.InvalidTokenError:
                    return 'Invalid token. Please log in again.', 401
        return 'Unauthorized', 401

    return decorated


@application.route('/', methods=['GET'])
def home():
    # Check if user is logged in
    if 'username' in session:
        return render_template('home.html', username=session['username'], exchange_data=exchange_data,
                               node_data=node_data)
    else:
        return redirect('/login')


@application.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')

        # Query the database for the user with the entered email
        user = UserInformation.query.filter_by(user_email=email).first()

        if user:
            session['username'] = email  # Store username in session
            token = jwt.encode({'username': email}, secrets.token_hex(16), algorithm='HS256')
            session['token'] = token  # Store token in session
            return redirect('/')
        else:
            return render_template('login.html', error=True)

    return render_template('login.html', error=False)




@application.route('/registration', methods=['GET', 'POST'])
def registration():
    if request.method == 'POST':
        user_name = request.form['username']
        user_email = request.form['email']
        user_password = request.form['password']

        response = is_weak_password(user_password)
        # print(response)

        statement = ('The password is too weak: \n'
                     'A strong password at least should have: \n'
                     '* 8 to 20 characters \n'
                     '* One upper case letter\n'
                     '* One lower case letter\n'
                     '* One special character\n'
                     '* One digit')

        if is_weak_password(user_password):
            return render_template('/home/ec2-user/environment/cm-flask-web-app/templates/registration.html', error=statement)

        with application.app_context():
            # Check if the email is already registered
            if UserInformation.query.filter_by(user_email=user_email).first():
                return render_template('/home/ec2-user/environment/cm-flask-web-app/templates/registration.html', error='Email already registered')

            # Create a new user instance
            new_user = UserInformation(user_name=user_name, user_email=user_email, user_password=user_password)

            # Add the user to the database
            db.session.add(new_user)
            db.session.commit()

        # Redirect the user to the login page after successful registration
        return redirect(url_for('login'))
    else:
        return render_template('/home/ec2-user/environment/cm-flask-web-app/templates/registration.html')


@application.route('/logout')
def logout():
    session.pop('username', None)  # Remove username from session
    session.pop('token', None)  # Remove token from session
    return redirect('/login')


@application.route('/node', methods=['POST', 'GET'])
def node():
    if request.method == 'POST':
        data = request.get_json()  # Get the JSON data from the request
        for item in data:
            if 'Node' in item["name"]:
                node_data.append(item)  # Store the data in the node_data list
                print(node_data)
            elif 'Exchange' in item["name"]:
                exchange_data.append(item)
                print(exchange_data)
            return 'Successfully received POST request'
    elif request.method == 'GET':
        return render_template('/home/ec2-user/environment/cm-flask-web-app/node.html', node_data=node_data), render_template('/home/ec2-user/environment/cm-flask-web-app/templates/exchange.html', exchange_data=exchange_data)

#######testing purposes
@application.route('/hexowatchexchange', methods=['POST', 'GET'])
def hexowatchexchange():
    if request.method == 'POST':
        data = request.get_json()  # Get the JSON data from the request
        for item in data:
            if 'Node' in item["name"]:
                node_data.append(item)  # Store the data in the node_data list
                print(node_data)
            elif 'Exchange' in item["name"]:
                exchange_data.append(item)
                print(exchange_data)
            return 'Successfully received POST request'
    elif request.method == 'GET':
        try:    
            return render_template('/home/ec2-user/environment/cm-flask-web-app/node.html', node_data=exchange_data), render_template('/home/ec2-user/environment/cm-flask-web-app/templates/exchange.html', exchange_data=exchange_data)
        except Exception as e:
            print(e)
            
#######testing purposes finish


@application.route('/webhook', methods=['POST', 'GET'])
def webhook():
    global node_data, exchange_data  # Declare the global variables

    if request.method == 'POST':
        data = request.get_json()  # Get the JSON data from the request
        for item in data:
            if item.get('name') == 'Node':
            
                #
                nodecontent = SecondaryModel(
                    exch_title=item['title'],
                    exch_name=item['name'],
                    exch_monitoredUrl=item['monitoredUrl'],
                    exch_createDate=item['createdDate'],
                    exch_date=item['date'],
                    exch_link=item['link'],
                    exch_diffData=item['diff_data'],
                    exch_newData=item['new_data'],
                    exch_oldData=item['old_data']
                )
                db.session.add(nodecontent)
                db.session.commit()
                #
                node_data.append(item)  # Store the data in the global node_data list
                # print(item)
            elif item.get('name') == 'Exchange':
                print(item)
                exchcontent = SecondaryModel(
                    exch_title=item['title'],
                    exch_name=item['name'],
                    exch_monitoredUrl=item['monitoredUrl'],
                    exch_createDate=item['createdDate'],
                    exch_date=item['date'],
                    exch_link=item['link'],
                    exch_diffData=item['diff_data'],
                    exch_newData=item['new_data'],
                    exch_oldData=item['old_data']
                )
                db.session.add(exchcontent)
                db.session.commit()
                exchange_data.append(item)  # Store the data in the global webhook_data list
                # print(item)
        return 'Successfully received POST request'
    elif request.method == 'GET':
        # Handle GET request
        data = request.get_json()
        for item in data:
            if item.get('name') == 'Node':
                return render_template('/home/ec2-user/environment/cm-flask-web-app/node.html', node_data=node_data)
            elif item.get('name') == 'Exchange':
                exchange_data.append(item)
                return render_template('/home/ec2-user/environment/cm-flask-web-app/templates/exchanges.html', exchange_data=exchange_data)

@application.route('/hexowatchexchange', methods=['GET'])
def get_webhook_data():
    exchange_data.append(jsonify(exchange_data))
    #print(len(exchange_data))
    #print(exchange_data[0])
    return jsonify(exchange_data)

@application.route('/exchanges', methods=['GET'])
def exchanges():
    exchanges = SecondaryModel.query.all()
    return render_template('/home/ec2-user/environment/cm-flask-web-app/templates/exchange.html', exchanges=exchanges)



@application.route('/node', methods=['GET'])
def get_node_data():
    node_data.append(jsonify(node_data))
    # print(len(node.data))
    # print(node.data[0])
    return jsonify(node_data)


if __name__ == '__main__':
    application.app_context().push()
    print(os.getcwd())
    application.debug = True
    application.run(host='0.0.0.0', port=8080)