from application import application, db, SecondaryModel
from flask_migrate import Migrate

with application.app_context():
    db.create_all()

    exit(5)

    def get_column_names(table_name):
        table = db.metadata.tables[table_name]
        column_names = [column.name for column in table.columns]
        return column_names

    # Example usage
    column_names = get_column_names()
    print(column_names)

    exit(5)

    def get_entries(table_name):
        table = db.metadata.tables[table_name]
        entries = db.session.query(table).all()
        return entries


    entries = get_entries('User Database')
    for entry in entries:
        print(entry.user_id, entry.user_name, entry.user_email, entry.user_password)

    # Example usage
    column_names = get_column_names('Historical Data')
    print(column_names)
