import sys
import json

data = sys.stdin.read()

data_list = json.loads(data)

print(data_list)

id = data_list['id']
title = data_list['title']
name = data_list['name']
monitoredUrl = data_list['monitoredUrl']
CreatedDate = data_list['CreatedDate']
date = data_list['date']
link = data_list['link']
diff_data = data_list['diff_data']
new_data = data_list['new_data']
old_data = data_list['old_data']