# Terraform scripts for Monitoring Cluster in AWS

## Modules
- Network : Creates a VPC, subnets, NAT, IG and RTs
- IAM : Creates and manages IAM users,roles and policies
- EKS : Creates a cluster with self-managed node group


## Prerequisites

Open your terminal and run:

```bash
export AWS_ACCESS_KEY_ID="your aws access key"
export AWS_SECRET_ACCESS_KEY="your aws secret key"
export AWS_DEFAULT_REGION="aws region you wish to deploy the cluster to"
```

Terraform version should be > 1.0

