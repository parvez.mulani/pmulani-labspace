output "sre_iam_role_arn" {
  description = "ARN of IAM role"
  value       = aws_iam_role.sre_admin_role.arn
}

output "sre_iam_role_name" {
  description = "Name of IAM role"
  value       = aws_iam_role.sre_admin_role.name
}

output "sre_iam_role_path" {
  description = "Path of IAM role"
  value       = aws_iam_role.sre_admin_role.path
}

output "sre_iam_role_unique_id" {
  description = "Unique ID of IAM role"
  value       = aws_iam_role.sre_admin_role.unique_id
}

output "sre_iam_user_name" {
  description = "The user's name"
  value       = aws_iam_user.sre_admin_user.name
}

output "sre_iam_user_arn" {
  description = "The ARN assigned by AWS for this user"
  value       = aws_iam_user.sre_admin_user.arn
}

output "sre_iam_user_unique_id" {
  description = "The unique ID assigned by AWS"
  value       = aws_iam_user.sre_admin_user.unique_id
}

output "sre_iam_role_policy_name" {
  description = "SRE-Role policy name"
  value       = aws_iam_role_policy.sre_admin_policy.name
}

output "sre_iam_role_policy_arn" {
  description = "SRE Role policy ARN"
  value       = aws_iam_role_policy.sre_admin_policy.id
}

output "sre_iam_user_policy_name" {
  description = "SRE-User policy name"
  value       = aws_iam_user_policy.sre_admin_user_policy.name
}

output "sre_iam_user_policy_arn" {
  description = "SRE-User policy ARN"
  value       = aws_iam_user_policy.sre_admin_user_policy.id
}
