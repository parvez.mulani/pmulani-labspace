#USER
resource "aws_iam_user" "sre_admin_user" {
  name = "sre_admin"
  path = "/"

  tags = {
    Project = var.stack
    Environment = var.env
    ManagedBy = "terraform"
    Team      = "SRE"
    Contact     = "parvez.mulani@coinmetrics.io"
  }
}

resource "aws_iam_access_key" "sre_admin_user_ak" {
  user = aws_iam_user.sre_admin_user.name
}

data "aws_iam_policy_document" "sre_admin_user_policy_doc" {
  statement {
    effect    = "Allow"
    actions   = ["*"]
    resources = ["*"]
  }
}

resource "aws_iam_user_policy" "sre_admin_user_policy" {
  name   = "sre_admin_user_policy"
  user   = aws_iam_user.sre_admin_user.name
  policy = data.aws_iam_policy_document.sre_admin_user_policy_doc.json
}

#ROLE
data "aws_iam_policy_document" "assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "sre_admin_role" {
  name               = "sre-admin"
  assume_role_policy = data.aws_iam_policy_document.assume-role-policy.json
}


resource "aws_iam_role_policy" "sre_admin_policy" {
  name = "admin_policy"
  role = aws_iam_role.sre_admin_role.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "*",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

