module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 19.0"

  cluster_name    = "sre-test-cluster"
  cluster_version = "1.27"

  cluster_endpoint_public_access  = true

  cluster_addons = {
    kube-proxy = {
      most_recent = true
    }
    vpc-cni = {
      most_recent = true
    }
  }

  vpc_id                   = "vpc-0d3a2293a1355e0a3"
  subnet_ids               = ["subnet-05a3c9569698def86", "subnet-00b63a5600d787a87", "subnet-0549484e05c0fca3b"]
  control_plane_subnet_ids = ["subnet-02e09fcc0be0b5e8d", "subnet-00daf1e9269132ca5", "subnet-04e81a2b3ee93cadd"]

  # Self Managed Node Group(s)
  self_managed_node_group_defaults = {
    instance_type                          = "t2.large"
    update_launch_template_default_version = true
    iam_role_additional_policies = {
      AmazonSSMManagedInstanceCore = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
    }
  }

  self_managed_node_groups = {
    one = {
      name         = "k8s-${var.stack}-ng"
      max_size     = 5
      desired_size = 1

      use_mixed_instances_policy = true
      mixed_instances_policy = {
        instances_distribution = {
          on_demand_base_capacity                  = 0
          on_demand_percentage_above_base_capacity = 10
          spot_allocation_strategy                 = "capacity-optimized"
        }

        override = [
          {
            instance_type     = "m5.large"
            weighted_capacity = "1"
          },
          {
            instance_type     = "t3.large"
            weighted_capacity = "2"
          },
        ]
      }
    }
  }
  # aws-auth configmap
  create_aws_auth_configmap = true
  manage_aws_auth_configmap = true

  aws_auth_roles = [
    {
      rolearn  = "arn:aws:iam::249324333018:role/sre-admin"
      username = "sre-admin"
      groups   = ["system:masters"]
    },
  ]

  aws_auth_users = [
    {
      userarn  = "arn:aws:iam::249324333018:user/sre_admin"
      username = "sre-admin"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::249324333018:user/pmulani-test"
      username = "pmulani"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::249324333018:user/sre"
      username = "sre-admin"
      groups   = ["system:masters"]
    },

  ]

  aws_auth_accounts = [
    "249324333018",
  ]

  tags = {
    Project = var.stack
    Environment = var.env
    ManagedBy = "terraform"
    Team      = "SRE"
    Contact     = "parvez.mulani@coinmetrics.io"
    Terraform   = "true"
  }
}