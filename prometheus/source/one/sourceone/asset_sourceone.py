#!/usr/bin/env python3
from prometheus_client import start_http_server, Summary
import random
import time

# SourceOne time spent and requests.
REQUEST_TIME = Summary('request_processing_seconds', 'SourceOne : Time spent processing request')

# Decorate function with metric.
@REQUEST_TIME.time()
def process_request(t):
    """A dummy function that takes some time."""
    time.sleep(t)

if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(8000)
    # Generate some requests.
    while True:
        process_request(random.random())
