import json
import defaultdict

#### Commenting out temp as for testing we could just use the json file downloaded using FIRE server.

# PROMETHEUS = 'http://localhost:9090/'
# response =requests.get(PROMETHEUS + '/api/v1/query', params={'query': 'request_processing_seconds_sum'}).text
# parsed = json.loads(response)
# print(json.dumps(parsed, indent=4))

# Code from Vlad
with open('/Users/parvezmulani/Desktop/target.json', 'r') as f:
    data = json.load(f)

unique_types = set()
unique_sources = set()

sources_assets = dict()

for i in data.get('data').get('result'):
    if i.get('metric').get('env') == 'production' and i.get('metric').get('type') == 'EXTERNAL':
        print("{0}\t {1} {2}".format(
            i.get('metric').get('asset'),
            i.get('metric').get('type'),
            i.get('metric').get('source')
        )
        )
        unique_sources.add(i.get('metric').get('source'))
    unique_types.add(i.get('metric').get('type'))


for source in data.get('data').get('result'):
    if source.get('metric').get('env') == 'production' and source.get('metric').get('type') == 'EXTERNAL':
        if source.get('metric').get('source') in unique_sources:
            sources_assets[source.get('metric').get('source')].append(source.get('metric').get('asset'))
        else:
            sources_assets[source.get('metric').get('source')] = [source.get('metric').get('asset')]

print(sources_assets)
    #print("Unique types: {0}".format(unique_types))
#print('\n'.join('{}: {}'.format(*k) for k in enumerate(unique_sources)))
