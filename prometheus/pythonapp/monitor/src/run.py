import json
with open('target.json', 'r') as f:
    data = json.load(f)

unique_types = set()

for i in data.get('data').get('result'):
    if i.get('metric').get('env') == 'production' and i.get('metric').get('type') == 'EXTERNAL':
        print("{0}\t {1} {2}".format(
            i.get('metric').get('asset'),
            i.get('metric').get('type'),
            i.get('metric').get('source'),
        )
        )
        unique_types.add(i.get('metric').get('type'))

print("Unique types: {0}".format(unique_types))